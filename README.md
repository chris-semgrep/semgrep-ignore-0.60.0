# semgrep-ignore-0.60.0

This project contains two 2 python files where one has an inline comment used to allow semgrep to ignore findings, while the other does not. This project is tested using the analyzer image 2.10 that uses semgrep version 0.60.0.
